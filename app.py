import os
from extensions import db

from flask import Flask, request, render_template, jsonify

app = Flask(__name__)
app.config.from_pyfile('config.py')


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/about-me')
def about():
    return render_template('about.html')


@app.route('/contact')
def contact():
    return render_template('contact.html')


@app.route('/study-abroad')
def study_abroad():
    return render_template('')


@app.route('/all-movies', methods=['GET'])
def read_all_posts():
    col = db.movieDetails
    output = []

    for i in col.find():
        output.append({'title': i['title']})

    return jsonify({'result': output})


@app.route('/all_movies/<movie>', methods=['GET'])
def read_post(movie):
    col = db.movieDetails
    s = col.find_one({'title': movie})

    if s:
        output = {'title': s['title'], 'year': s['year']}
    else:
        output = 'No post like this'

    return jsonify({'result': output})


@app.route('/all_movies', methods=['POST'])
def write_post():
    col = db.movieDetails
    title = request.json['title']
    body = request.json['body']
    new_id = col.insert_one({'title': title, 'year': body})
    new_title = col.find_one({'_id': new_id})
    output = {'title': new_title['title'], 'year': new_title['year']}
    return jsonify({'result': output})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
